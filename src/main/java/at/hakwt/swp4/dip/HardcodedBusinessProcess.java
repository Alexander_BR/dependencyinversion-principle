package at.hakwt.swp4.dip;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of a {@link BusinessProcess} which does not allow
 * to be configured externally. It configures its steps in the constructor
 * and that's it. There is no way in using this class with different steps.
 */
public class HardcodedBusinessProcess implements BusinessProcess {

    private final List<BusinessProcessStep> steps;

    public HardcodedBusinessProcess() {
        this.steps = new ArrayList<>();
        this.steps.add(new LoadFromCsvStep());
        this.steps.add(new DataCanBeStoredStep());
        this.steps.add(new StoreDataStep());
    }

    @Override
    public void run() {
        for(BusinessProcessStep step : steps) {
            step.process();
        }
    }

    @Override
    public void configure(List<BusinessProcessStep> steps) {
        // do nothing because configuration already happens in the constructor
    }

}
