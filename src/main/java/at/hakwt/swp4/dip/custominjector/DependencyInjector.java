package at.hakwt.swp4.dip.custominjector;

import at.hakwt.swp4.dip.BusinessProcessStep;

import java.lang.reflect.InvocationTargetException;

/**
 * Creates concrete instances of implementations of {@link BusinessProcessStep}s.
 *
 * This is a very simple version of a dependency injector. More sophisticated versions
 * like the Spring Framework or Google Guice can be used as a library.
 */
public class DependencyInjector {

    public BusinessProcessStep createInstanceOf(Class c) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Object o = c.getDeclaredConstructor().newInstance();
        if ( o instanceof BusinessProcessStep) {
            return (BusinessProcessStep) o;
        }
        throw new IllegalArgumentException("c must implement BusinessProcessStep");
    }


}
