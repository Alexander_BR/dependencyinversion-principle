package at.hakwt.swp4.dip;

public class DataCanBeStoredStep implements BusinessProcessStep {

    @Override
    public void process() {
        System.out.println("Check if data can be stored");
    }
}
