package at.hakwt.swp4.dip.springframework;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SocialMediaMain
{
public static void main(String[] Args)
{
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("social-media-context.xml");

    SocialMediaManager manager = applicationContext.getBean("socialMediaManager", SocialMediaManager.class);

    manager.send("New MacBookPro available!");
}
}
