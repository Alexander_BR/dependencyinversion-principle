package at.hakwt.swp4.dip.springframework;

public class TwitterSender implements SocialMediaSender {
    @Override
    public void sendMessage(String message)
    {
        System.out.println("sending message " + message + " to Twitter!");
    }
}
