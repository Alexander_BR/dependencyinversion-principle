package at.hakwt.swp4.dip.springframework;

import java.util.List;

public class SocialMediaManager
{
private List<SocialMediaSender> senders;

public SocialMediaManager(List<SocialMediaSender> senders)
{
    this.senders = senders;
}

public void send(String message)
{
    for (SocialMediaSender sender:
         this.senders)
    {
        sender.sendMessage(message);

    }
}
}
